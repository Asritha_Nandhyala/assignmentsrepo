package com.hcl.main;

import com.hcl.model.Swap;

public class swapMain {

	public static void main(String[] args) {
		Swap swap = new Swap();
		swap.number1 = 21;
		swap.number2 = 20;
		System.out.println("befor swaping:");
		System.out.println(swap.number1);
		System.out.println(swap.number2);
		swap.number1 = swap.number1 + swap.number2;
		swap.number2 = swap.number1 - swap.number2;
		swap.number1 = swap.number1 - swap.number2;
		System.out.println("after swaping:");
		System.out.println(swap.number1);
		System.out.println(swap.number2);

	}

}
