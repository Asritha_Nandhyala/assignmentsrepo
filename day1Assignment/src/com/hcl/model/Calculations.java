package com.hcl.model;

public class Calculations {
	public int findaddition(int num1, int num2) {
		return num1 + num2;
	}

	public int findsubtraction(int num1, int num2) {
		return num1 - num2;
	}
	public int findmultiply(int num1, int num2) {
		return num1 * num2;
	}

	public int finddivision(int num1, int num2) {
		return num1 / num2;
	}

	public int findremainder(int num1, int num2) {
		return num1 % num2;
	}
}
