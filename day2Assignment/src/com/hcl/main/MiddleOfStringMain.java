package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.MiddleOfString;

public class MiddleOfStringMain {

	public static void main(String[] args) {
		 Scanner in = new Scanner(System.in);
		 MiddleOfString middleOfString=new MiddleOfString();
		 
	        System.out.print("Input a string: ");
	        String str = in.nextLine();
	        System.out.print("The middle character in the string: "+middleOfString.middle(str) );
	}

}
