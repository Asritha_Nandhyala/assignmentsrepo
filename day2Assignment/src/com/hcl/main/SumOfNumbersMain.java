package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.SumOfNumbers;

public class SumOfNumbersMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		SumOfNumbers numbers = new SumOfNumbers();
		System.out.print("Enter the number of elements you want to store: ");

		int n = scanner.nextInt();
		System.out.println("Enter the numbers");

		int[] array = new int[10];

		for (int i = 0; i < n; i++) {

			array[i] = scanner.nextInt();

		}
		System.out.println("Sum of given numbers is:" + numbers.add(array));

	}

}
