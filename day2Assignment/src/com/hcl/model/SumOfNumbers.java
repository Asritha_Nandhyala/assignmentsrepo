package com.hcl.model;

import java.util.Arrays;

/*Your task is to create the class Addition and the required methods 
 * so that the code prints  the sum of the numbers passed to the function addition. */
public class SumOfNumbers {

	public int add(int[] numbers) {
		/* return Arrays.stream(numbers).sum(); */
		int sum = 0;
		for (int i : numbers) {

			sum += i;

		}

		return sum;

	}
}
