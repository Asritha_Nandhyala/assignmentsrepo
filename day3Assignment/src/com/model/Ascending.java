package com.model;
/*write a Java program to sort an integer array of 10 elements in ascending. */

public class Ascending {
	public static void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	public static void sortArray(int[] array) {
		int temporary = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] > array[j]) {
					temporary = array[i];
					array[i] = array[j];
					array[j] = temporary;
				}
			}
		}
		printArray(array);

	}

}
