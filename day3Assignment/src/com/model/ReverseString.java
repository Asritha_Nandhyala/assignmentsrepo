package com.model;

/*Write a program to read a string and a character, 
 * and reverse the string and convert it in a format such 
that each character is separated by the given character. Print the final string.*/
public class ReverseString {
	public String wordReverse(String str) {

		{
			String rev = "";
			for (int i = str.length(); i > 0; --i) {
				rev = rev + (str.charAt(i - 1));

			}

			return rev;
		}
	}
}
