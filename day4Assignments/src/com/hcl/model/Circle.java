package com.hcl.model;

public class Circle extends Shape {
	private final double PI = 3.14;
	private float radius;

	public Circle(String name, float radius) {
		super(name);
		this.radius = radius;
	}

	public Circle(String circle) {
		super(circle);
		// TODO Auto-generated constructor stub
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public double getPI() {
		return PI;
	}

	@Override
	public float calculateArea(float radius) {
		float area;
		return area = (float) (PI * radius * radius);
	}

	@Override
	public float calculateArea() {
		// TODO Auto-generated method stub
		return 0;
	}

}
