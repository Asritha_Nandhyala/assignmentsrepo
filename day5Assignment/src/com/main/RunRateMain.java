package com.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import com.service.RunRateService;

public class RunRateMain {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
		RunRateService runRate = new RunRateService();
		try {
			System.out.println("Enter Total Runs Scored:");
			int runs = Integer.parseInt(buffer.readLine());
			System.out.println("Enter Total Overs Faced:");
			int overs = Integer.parseInt(buffer.readLine());
			System.out.println("Current RunRate is:" + runRate.runRate(runs, overs));

		} catch (Exception e) {
			System.out.println("Exception" + e);
		} finally {
			buffer = null;
			runRate = null;
		}

	}
}
