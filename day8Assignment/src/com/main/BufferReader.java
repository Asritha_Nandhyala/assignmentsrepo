package com.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BufferReader {

	public static void main(String[] args) {
		InputStreamReader r = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(r);
		System.out.println("Enter your name");
		String name = null;
		try {
			name = br.readLine();
		} catch (IOException e) {

			e.printStackTrace();
		}
		System.out.println("Hello " + name + "!");
	}

}
